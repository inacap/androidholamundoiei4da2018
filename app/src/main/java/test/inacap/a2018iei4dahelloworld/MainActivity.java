package test.inacap.a2018iei4dahelloworld;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import test.inacap.a2018iei4dahelloworld.controlador.UsuariosController;

public class MainActivity extends AppCompatActivity {

    public EditText etNombreUsuario, etPass;
    public Button btnIngresar;
    public TextView tvMensaje, tvRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Validar sesion
        SharedPreferences sesiones = getSharedPreferences("sesiones", Context.MODE_PRIVATE);
        boolean inicioSesion = sesiones.getBoolean("inicioSesion", false);
        if(inicioSesion){
            Intent i = new Intent(MainActivity.this, HomeDrawerActivity.class);
            startActivity(i);
            finish();
        }


        this.etNombreUsuario = findViewById(R.id.etNombreUsuario);
        this.etPass = findViewById(R.id.etContrasena);
        this.btnIngresar = findViewById(R.id.btnIngresar);
        this.tvMensaje = findViewById(R.id.tvMensaje);
        this.tvRegistrar = findViewById(R.id.tvRegistrar);

        this.tvRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Intent
                Intent intentNuevaVentana = new Intent(MainActivity.this, RegistrarUsuarioActivity.class);
                startActivity(intentNuevaVentana);
            }
        });

        this.btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Capturar los datos y luego mostrarlos
                String nombreUsuario = etNombreUsuario.getText().toString();
                String password = etPass.getText().toString();

                UsuariosController controller = new UsuariosController(getApplicationContext());
                int inicioSesion = controller.login(nombreUsuario, password);

                if(inicioSesion == 0){
                    // Inicio de sesion

                    // SharedPreferences
                    SharedPreferences sesiones = getSharedPreferences("sesiones", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sesiones.edit();
                    editor.putBoolean("inicioSesion", true);
                    editor.commit();


                    Intent i = new Intent(MainActivity.this, HomeDrawerActivity.class);
                    startActivity(i);
                    finish();

                }
            }
        });
    }
}







