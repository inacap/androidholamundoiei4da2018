package test.inacap.a2018iei4dahelloworld.controlador;

import android.content.ContentValues;
import android.content.Context;

import test.inacap.a2018iei4dahelloworld.modelo.MainDBContract;
import test.inacap.a2018iei4dahelloworld.modelo.UsuariosModel;

public class UsuariosController {
    private UsuariosModel capaModelo;

    public UsuariosController(Context ctx){
        this.capaModelo = new UsuariosModel(ctx);
    }

    public void crearUsuario(String uName, String p1, String p2) throws Exception{
        // Validar los datos
        if(!p1.equals(p2)){
            // Error en las passwords
            // Lanzar el error
            throw new Exception("Las contraseñas no coinciden.");
        }

        // Crear el usuario
        ContentValues usuario = new ContentValues();
        usuario.put(MainDBContract.MainDBUsuarios.COLUMNA_USERNAME, uName);
        usuario.put(MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD, p1);

        this.capaModelo.crearUsuario(usuario);

    }

    public int login(String username, String password){

        // Devolver 0 significa que inicio sesion
        // Pedir datos de usuario
        ContentValues usuario = this.capaModelo.obtenerUsuarioPorUsername(username);

        // Verificar si existe
        if(usuario == null){
            // NO existe
            return 2;
        }

        // verificar password
        String passDB = usuario.getAsString(MainDBContract.MainDBUsuarios.COLUMNA_PASSWORD);
        if(passDB.equals(password)){
            // OK
            return 0;
        }

        return 1;
    }
}
