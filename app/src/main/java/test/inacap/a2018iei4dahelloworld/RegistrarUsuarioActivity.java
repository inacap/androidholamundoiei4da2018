package test.inacap.a2018iei4dahelloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import test.inacap.a2018iei4dahelloworld.controlador.UsuariosController;

public class RegistrarUsuarioActivity extends AppCompatActivity {

    public EditText etUserName, etPass1, etPass2;
    public TextView tvMensaje;
    public Button btRegistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_usuario);

        etUserName = (EditText) findViewById(R.id.etUsername);
        etPass1 = (EditText) findViewById(R.id.etPassword1);
        etPass2 = (EditText) findViewById(R.id.etPassword2);

        tvMensaje = (TextView) findViewById(R.id.tvMensaje);

        btRegistrar = (Button) findViewById(R.id.btRegistrar);

        btRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Capturar los datos
                String userName = etUserName.getText().toString();
                String password1 = etPass1.getText().toString();
                String password2 = etPass2.getText().toString();

                UsuariosController capaControlador = new UsuariosController(getApplicationContext());
                try {
                    capaControlador.crearUsuario(userName, password1, password2);
                } catch (Exception e) {
                    String mensaje = e.getMessage();
                    tvMensaje.setText(mensaje);
                }
            }
        });

    }
}
